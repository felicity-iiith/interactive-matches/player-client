# player_client.PlayerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_action**](PlayerApi.md#get_action) | **POST** / | Get the action from submitted code
[**init**](PlayerApi.md#init) | **POST** /init | Initialize the player
[**quit**](PlayerApi.md#quit) | **POST** /quit | Quit this server


# **get_action**
> InlineResponse2001 get_action(inline_object1)

Get the action from submitted code

It's this player's turn! The request will contain the current state. This submission's action is expected as the result

### Example

```python
from __future__ import print_function
import time
import player_client
from player_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = player_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with player_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = player_client.PlayerApi(api_client)
    inline_object1 = player_client.InlineObject1() # InlineObject1 | 

    try:
        # Get the action from submitted code
        api_response = api_instance.get_action(inline_object1)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlayerApi->get_action: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object1** | [**InlineObject1**](InlineObject1.md)|  | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successfully extracted action from code |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **init**
> InlineResponse200 init(inline_object)

Initialize the player

Initializes the player by compiling source code (for C/C++) or importing python module and starts the runner

### Example

```python
from __future__ import print_function
import time
import player_client
from player_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = player_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with player_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = player_client.PlayerApi(api_client)
    inline_object = player_client.InlineObject() # InlineObject | 

    try:
        # Initialize the player
        api_response = api_instance.init(inline_object)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlayerApi->init: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object** | [**InlineObject**](InlineObject.md)|  | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successfully initialized the player |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **quit**
> str quit()

Quit this server

On an invalid move/timeout/the policy set by the game setter, this player is no longer eligible to play. Shutting down server to reduce resource usage

### Example

```python
from __future__ import print_function
import time
import player_client
from player_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = player_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with player_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = player_client.PlayerApi(api_client)
    
    try:
        # Quit this server
        api_response = api_instance.quit()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlayerApi->quit: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Starts the process of shutting down |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

