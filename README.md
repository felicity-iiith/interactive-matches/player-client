# player-client
This is the server managing submitted code and actions on them

This Python package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 0.0.2
- Package version: 0.0.1
- Build package: org.openapitools.codegen.languages.PythonLegacyClientCodegen

## Requirements.

Python 2.7 and 3.4+

## Installation & Usage
### pip install

If the python package is hosted on a repository, you can install directly using:

```sh
pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git`)

Then import the package:
```python
import player_client
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import player_client
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python
from __future__ import print_function

import time
import player_client
from player_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = player_client.Configuration(
    host = "http://localhost"
)



# Enter a context with an instance of the API client
with player_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = player_client.PlayerApi(api_client)
    inline_object1 = player_client.InlineObject1() # InlineObject1 | 

    try:
        # Get the action from submitted code
        api_response = api_instance.get_action(inline_object1)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PlayerApi->get_action: %s\n" % e)
    
```

## Documentation for API Endpoints

All URIs are relative to *http://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*PlayerApi* | [**get_action**](docs/PlayerApi.md#get_action) | **POST** / | Get the action from submitted code
*PlayerApi* | [**init**](docs/PlayerApi.md#init) | **POST** /init | Initialize the player
*PlayerApi* | [**quit**](docs/PlayerApi.md#quit) | **POST** /quit | Quit this server


## Documentation For Models

 - [InlineObject](docs/InlineObject.md)
 - [InlineObject1](docs/InlineObject1.md)
 - [InlineResponse200](docs/InlineResponse200.md)
 - [InlineResponse2001](docs/InlineResponse2001.md)


## Documentation For Authorization

 All endpoints do not require authorization.

## Author

yoogottam.khandelwal@students.iiit.ac.in


